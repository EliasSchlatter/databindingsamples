package com.mobilab.sampleandroiddatabinding.callback;

import com.mobilab.sampleandroiddatabinding.model.User;

/**
 * @author Elias
 */

public interface MainActivityCallback {
    void onUserClicked(User user);
}
