package com.mobilab.sampleandroiddatabinding.callback;

import com.mobilab.sampleandroiddatabinding.model.User;

/**
 * @author Elias
 */

public interface UserItemCallback {

    void onUserClicked(User user);
}
