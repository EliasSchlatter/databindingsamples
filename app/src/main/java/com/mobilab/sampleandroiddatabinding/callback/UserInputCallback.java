package com.mobilab.sampleandroiddatabinding.callback;

import com.mobilab.sampleandroiddatabinding.model.User;

/**
 * @author Elias
 */

public interface UserInputCallback {
    void addUser(User user);

    void updateUser(User user);
}
