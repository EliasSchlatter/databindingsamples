package com.mobilab.sampleandroiddatabinding;

import com.mobilab.sampleandroiddatabinding.model.User;

/**
 * @author Elias
 */

public class UserInputFragmentViewModel {

    private User mUser;

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }
}
