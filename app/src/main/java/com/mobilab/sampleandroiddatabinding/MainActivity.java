package com.mobilab.sampleandroiddatabinding;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.mobilab.sampleandroiddatabinding.adapter.MyViewPagerAdapter;
import com.mobilab.sampleandroiddatabinding.callback.MainActivityCallback;
import com.mobilab.sampleandroiddatabinding.callback.UserInputCallback;
import com.mobilab.sampleandroiddatabinding.callback.UserListCallback;
import com.mobilab.sampleandroiddatabinding.model.User;

import java.util.ArrayList;
import java.util.List;


/**
 * This is a Sample that describes how to use dataBinding.
 * <p>
 * It covers the following topics:
 * <p>
 * - how to bind a value to a view
 * - how to bind a method to a view event (method references)
 * - how to bind a method to a view event (listener bindings)
 * <p>
 * <p>
 * Here I will use dataBinding to:
 * - write data from view to viewModel
 * - write data from viewModel to view
 * - call methods from the xml
 */
public class MainActivity extends AppCompatActivity implements UserInputCallback, MainActivityCallback {


    private ViewPager mViewPager;

    private UserListCallback mUserListCallback;

    private UserListFragment userListFragment;
    private UserInputFragment userInputFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout.setupWithViewPager(mViewPager);

        userListFragment = new UserListFragment();
        userListFragment.setCallback(this);
        userInputFragment = new UserInputFragment();
        userInputFragment.setCallback(this);

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(userInputFragment);
        fragments.add(userListFragment);

        MyViewPagerAdapter pagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(pagerAdapter);

        User user = new User();
        UserInputFragmentViewModel userInputFragmentViewModel = new UserInputFragmentViewModel();
        userInputFragmentViewModel.setUser(user);
    }

    public void setUserListCallback(UserListCallback callback) {
        mUserListCallback = callback;
    }

    @Override
    public void addUser(User user) {
        mUserListCallback.addUser(user);
        mViewPager.setCurrentItem(1);
    }

    @Override
    public void updateUser(User user) {
        mUserListCallback.updateUser(user);
        mViewPager.setCurrentItem(1);
    }

    @Override
    public void onUserClicked(User user) {
        userInputFragment.setUser(user);
        mViewPager.setCurrentItem(0);
    }
}
