package com.mobilab.sampleandroiddatabinding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.mobilab.sampleandroiddatabinding.adapter.MyListAdapter;
import com.mobilab.sampleandroiddatabinding.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Elias
 */

public class UserListFragmentViewModel extends BaseObservable {

    private List<User> mUsers = new ArrayList<>();
    private MyListAdapter adapter;

    @Bindable
    public List<User> getUsers() {
        return mUsers;
    }

    public void setUsers(List<User> users) {
        mUsers = users;
        notifyPropertyChanged(BR.users);
        adapter.notifyDataSetChanged();
    }

    @Bindable
    public MyListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(MyListAdapter adapter) {
        this.adapter = adapter;
        notifyPropertyChanged(BR.adapter);
    }
}
