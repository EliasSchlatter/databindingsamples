package com.mobilab.sampleandroiddatabinding.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

import java.util.UUID;

/**
 * @author Elias
 */

public class User extends BaseObservable {

    private UUID id;

    private String imageUrl;

    private String firstName = "";
    private String lastName = "";
    private String namePreview;

    private String mStreet = "";
    private String mNumber = "";
    private String mCity = "";
    private String mZip = "";
    private String addressPreviewFirstLine;
    private String addressPreviewSecondLine;

    private String birthDay = "";
    private String birthMonth = "";
    private String birthYear = "";
    private String birthPreview;

    public User() {
    }

    @Bindable
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(BR.lastName);
        setNamePreview();
    }

    @Bindable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(BR.firstName);
        setNamePreview();
    }

    @Bindable
    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
        notifyPropertyChanged(BR.birthDay);
        setBirthPreview();
    }

    @Bindable
    public String getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
        notifyPropertyChanged(BR.birthMonth);
        setBirthPreview();
    }

    @Bindable
    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
        notifyPropertyChanged(BR.birthYear);
        setBirthPreview();
    }

    @Bindable
    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
        notifyPropertyChanged(BR.street);
        setAddressPreviewFirstLine();
    }

    @Bindable
    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
        notifyPropertyChanged(BR.number);
        setAddressPreviewFirstLine();
    }

    @Bindable
    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
        notifyPropertyChanged(BR.city);
        setAddressPreviewSecondLine();
    }

    @Bindable
    public String getZip() {
        return mZip;
    }

    public void setZip(String zip) {
        mZip = zip;
        notifyPropertyChanged(BR.zip);
        setAddressPreviewSecondLine();
    }

    @Bindable
    public String getNamePreview() {
        return namePreview;
    }

    @Bindable
    public String getAddressPreviewFirstLine() {
        return addressPreviewFirstLine;
    }

    @Bindable
    public String getAddressPreviewSecondLine() {
        return addressPreviewSecondLine;
    }

    @Bindable
    public String getBirthPreview() {
        return birthPreview;
    }

    private void setNamePreview() {
        namePreview = firstName + " " + lastName;
        notifyPropertyChanged(BR.namePreview);
    }

    private void setAddressPreviewFirstLine() {
        addressPreviewFirstLine = mStreet + " " + mNumber;
        notifyPropertyChanged(BR.addressPreviewFirstLine);
    }

    private void setAddressPreviewSecondLine() {
        addressPreviewSecondLine = mZip + " " + mCity;
        notifyPropertyChanged(BR.addressPreviewSecondLine);
    }

    private void setBirthPreview() {
        birthPreview = birthDay + "." + birthMonth + "." + birthYear;
        notifyPropertyChanged(BR.birthPreview);
    }

    @Bindable
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }
}
