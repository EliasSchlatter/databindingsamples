package com.mobilab.sampleandroiddatabinding.adapter;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * @author Elias
 */

public class BindingAdapters {

    @BindingAdapter(value = {"imageUrl", "placeholder"}, requireAll = false)
    public static void setImageUrl(ImageView imageView, String url, Drawable placeHolder) {
        if (url == null || url.equals("")) {
            imageView.setImageDrawable(placeHolder);
        } else {
            Picasso.with(imageView.getContext())
                    .load(url)
                    .placeholder(placeHolder)
                    .resize(500,500)
                    .into(imageView);
        }
    }
}
