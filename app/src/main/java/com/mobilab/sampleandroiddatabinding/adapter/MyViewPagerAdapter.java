package com.mobilab.sampleandroiddatabinding.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * @author Elias
 */

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragments;

    public MyViewPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "User Input";
            case 1:
                return "User List";
            default:
                throw new IllegalArgumentException();
        }
    }
}
