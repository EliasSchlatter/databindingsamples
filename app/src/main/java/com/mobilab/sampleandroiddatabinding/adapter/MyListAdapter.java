package com.mobilab.sampleandroiddatabinding.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mobilab.sampleandroiddatabinding.databinding.ItemUserBinding;
import com.mobilab.sampleandroiddatabinding.handler.UserItemHandler;
import com.mobilab.sampleandroiddatabinding.model.User;

import java.util.List;

import static com.mobilab.sampleandroiddatabinding.R.layout.item_user;

/**
 * @author Elias
 */

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {

    private List<User> mUsers;
    private LayoutInflater mLayoutInflater;
    private UserItemHandler mItemHandler;

    public MyListAdapter(List<User> users, UserItemHandler itemHandler) {
        mUsers = users;
        mItemHandler = itemHandler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        final ItemUserBinding binding = DataBindingUtil.inflate(mLayoutInflater, item_user, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mBinding.setUser(mUsers.get(position));
        holder.mBinding.setItemHandler(mItemHandler);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final ItemUserBinding mBinding;

        ViewHolder(ItemUserBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}
