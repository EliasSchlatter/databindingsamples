package com.mobilab.sampleandroiddatabinding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobilab.sampleandroiddatabinding.callback.UserInputCallback;
import com.mobilab.sampleandroiddatabinding.databinding.FragmentUserInputBinding;
import com.mobilab.sampleandroiddatabinding.handler.TextHandler;
import com.mobilab.sampleandroiddatabinding.model.User;

import java.util.UUID;

/**
 * @author Elias
 */

public class UserInputFragment extends android.support.v4.app.Fragment {

    private FragmentUserInputBinding binding;
    private UserInputFragmentViewModel viewModel;

    private UserInputCallback mCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_input, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = new User();
        viewModel = new UserInputFragmentViewModel();
        viewModel.setUser(user);

        // Then bind the user to it. User was declared within the dataTag in the xml file.
        binding.setViewModel(viewModel);
        binding.setFragment(this);
        binding.setHandler(new TextHandler(getActivity()));
    }

    public void setUser(User user) {
        viewModel.setUser(user);
        binding.setViewModel(viewModel);
    }

    public void setCallback(UserInputCallback callback) {
        mCallback = callback;
    }

    public void onSaveClicked(User user) {
        if (user.getId() == null) {
            user.setId(UUID.randomUUID());
            mCallback.addUser(user);
            clearViewModel();
        } else {
            mCallback.updateUser(user);
            clearViewModel();
        }
    }

    private void clearViewModel() {
        viewModel.setUser(new User());
        binding.setViewModel(viewModel);
    }
}
