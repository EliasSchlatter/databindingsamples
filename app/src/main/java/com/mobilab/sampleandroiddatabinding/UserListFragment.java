package com.mobilab.sampleandroiddatabinding;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobilab.sampleandroiddatabinding.adapter.MyListAdapter;
import com.mobilab.sampleandroiddatabinding.callback.MainActivityCallback;
import com.mobilab.sampleandroiddatabinding.callback.UserItemCallback;
import com.mobilab.sampleandroiddatabinding.callback.UserListCallback;
import com.mobilab.sampleandroiddatabinding.databinding.FragmentUserListBinding;
import com.mobilab.sampleandroiddatabinding.handler.UserItemHandler;
import com.mobilab.sampleandroiddatabinding.model.User;

import java.util.List;

/**
 * @author Elias
 */

public class UserListFragment extends Fragment implements UserListCallback, UserItemCallback {

    private final UserListFragmentViewModel mViewmodel = new UserListFragmentViewModel();
    private final UserItemHandler mItemHandler = new UserItemHandler(this);
    private MainActivityCallback mCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentUserListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewmodel.setAdapter(new MyListAdapter(mViewmodel.getUsers(), mItemHandler));

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mViewmodel.getAdapter());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            MainActivity activity = (MainActivity) context;
            activity.setUserListCallback(this);
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement UserListCallback");
        }
    }

    @Override
    public void addUser(User user) {
        List<User> users = mViewmodel.getUsers();
        users.add(user);
        mViewmodel.setUsers(users);
    }

    @Override
    public void updateUser(User user) {
        List<User> users = mViewmodel.getUsers();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() != null && users.get(i).getId() == user.getId()) {
                users.set(i, user);
            }
        }
        mViewmodel.setUsers(users);
    }

    public void setCallback(MainActivityCallback callback) {
        mCallback = callback;
    }

    @Override
    public void onUserClicked(User user) {
        mCallback.onUserClicked(user);
    }
}
