package com.mobilab.sampleandroiddatabinding.handler;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * @author Elias
 */

public class TextHandler {

    private final Activity mActivity;

    public TextHandler(Activity activity) {
        mActivity = activity;
    }

    public void focusNextView(View currentView, View nextView, int maxLetters) {
        EditText currentEditText = (EditText) currentView;
        EditText nextEditText = (EditText) nextView;
        if (currentEditText.getText().length() >= maxLetters) {
            nextEditText.requestFocus();
        }
    }

    public void hideKeyBoard(View currentView, int maxLetters) {
        EditText currentEditText = (EditText) currentView;
        if (currentEditText.getText().length() >= maxLetters) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(currentEditText.getWindowToken(), 0);
        }
    }
}
