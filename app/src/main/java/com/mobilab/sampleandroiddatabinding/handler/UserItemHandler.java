package com.mobilab.sampleandroiddatabinding.handler;

import com.mobilab.sampleandroiddatabinding.callback.UserItemCallback;
import com.mobilab.sampleandroiddatabinding.model.User;

/**
 * @author Elias
 */

public class UserItemHandler {

    private final UserItemCallback mCallback;

    public UserItemHandler(UserItemCallback callback) {
        mCallback = callback;
    }

    public void onUserClicked(User user) {
        mCallback.onUserClicked(user);
    }
}
